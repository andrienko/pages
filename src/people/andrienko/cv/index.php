<?php
   // Curriculum Vitae page
   define("_HOME_DIR", "../");
   define("_XHTML_CLASS_DIR", _HOME_DIR."classes/");
   define("_XHTML_CLASS", _XHTML_CLASS_DIR."cPage.php");

   if (is_readable(_XHTML_CLASS)) include_once(_XHTML_CLASS);

   $my_page = new cPage("", "Curriculum Vitae", _HOME_DIR."styles/index.css");

   $my_page_data = array (
                        "title"                 => "Curriculum Vitae",
                        "directory"             => _HOME_DIR,
                        "menu_file"             => "menu.txt",
                        "navigator_file"        => "menu.txt",
                        "content_file"          => "content.html",
                        "footer_text"           => "Denis Andrienko, 2005"
                    );

   $my_skipmenu = new cHeader($my_page_data);
   $my_page->add($my_skipmenu, "header");

   $my_navigator = new cNavigator($my_page_data);
   $my_page->add($my_navigator, "navigator");

   $my_menu = new cMenu($my_page_data);
   $my_menu->style = _HOME_DIR."styles/menu.css";
   $my_page->add($my_menu, "menu");

   $my_content = new cHTML($my_page_data);
   $my_content->style = _HOME_DIR."styles/cv.css";
   $my_page->add($my_content, "content");

   $my_footer = new cFooter($my_page_data);
   $my_page->add($my_footer, "footer");

   $my_page->display();


   ?>
