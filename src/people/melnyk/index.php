<?php
   //  Main page
   define("_HOME_DIR", "../../");
   define("_XHTML_CLASS_DIR", _HOME_DIR."classes/");
   define("_XHTML_CLASS", _XHTML_CLASS_DIR."cPage.php");

   if (is_readable(_XHTML_CLASS)) include_once(_XHTML_CLASS);

   $my_page = new cPage(_HOME_DIR, "Anton Melnyk", "styles/index.css");
   $my_page->icon = "images/ukraine.ico";
   
   $my_page_data = array (
                        "title"                 => "Anton Melnyk",
                        "directory"             => _HOME_DIR,
                        "menu_file"             => "menu.txt",
                        "navigator_file"        => "navigator.txt",
                        "content_file"          => "content.html",
                        "footer_text"           => "Denis Andrienko, 2005"
                    );

   $my_navigator = new cNavigator($my_page_data);
   $my_page->add($my_navigator, "navigator");

   $my_content = new cHTML($my_page_data);
   $my_page->add($my_content, "content");

   $my_right_data = array (
                        "directory"          => "../",
                        "list_file"          => "people.txt",
                    );
   $my_right_menu = new cCollection($my_right_data);
   $my_right_menu->id = "sidebar";
   $my_page->add($my_right_menu, "sidebar_container");

   $my_bib_data = array(
                        "directory" => "../../publications/",
                        "bib_file"  => "publications.bib"
                       ) ;

   $my_bib = new cBIB($my_bib_data);
   $my_bib->header = "Published in the group";
   $my_bib->id = "publications";
   $my_bib->style="styles/publications.css";
   $my_bib->filter_key="author";
   $my_bib->filter_value="Melnyk";
   $my_bib->home_dir="../../";
   $my_bib->output="compact";

   $my_page->add($my_bib, "content");
    

   $my_footer = new cFooter($my_page_data);
   $my_page->add($my_footer, "footer");

   $my_page->display();

?>
