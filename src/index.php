<?php

   //phpinfo();

   //  Main page
   define("_HOME_DIR", "");
   define("_XHTML_CLASS_DIR", _HOME_DIR."classes/");
   define("_XHTML_CLASS", _XHTML_CLASS_DIR."cPage.php");
                     
   if (is_readable(_XHTML_CLASS)) include_once(_XHTML_CLASS);
                            
   // spyware
   // define("_BBC_PAGE_NAME", "Welcome");
   // define("_BBCLONE_DIR", "spie/");
   // define("COUNTER", _BBCLONE_DIR."mark_page.php");
   // if (is_readable(COUNTER)) include_once(COUNTER);

   $my_page = new cPage(_HOME_DIR, "Welcome", "styles/index.css");

   $my_page->icon = "images/ukraine.ico";
   $my_page_data = array (
                        "title"                 => "Welcome",
                        "directory"             => _HOME_DIR,
                        "menu_file"             => "menu.txt",
                        "navigator_file"        => "navigator.txt",
                        "content_file"          => "content.html",
                        "footer_text"           => "Denis Andrienko, 2005"
                    );


   $my_navigator = new cNavigator($my_page_data);
   $my_page->add($my_navigator, "navigator");

   $my_right_data = array (
                        "directory"             => _HOME_DIR,
                        "list_file"          => "quick_links.txt",
                    );

   /*
   $slideshow_data = array (
                        "directory"             => _HOME_DIR,
                        "content_file"          => "slideshow.html",
    );

   $slideshow = new cHTML($slideshow_data);
   $slideshow->id = "sidebar";
   $my_page->add($slideshow, "sidebar_container");
    */

   $news_data = array (
                        "directory"             => _HOME_DIR,
                        "content_file"          => "news.html",
                    );
   $news = new cHtml($news_data);
   $news->id = "sidebar";
   $my_page->add($news, "sidebar_container");


   $my_right_menu = new cCollection($my_right_data);
   $my_right_menu->id = "sidebar";
   $my_page->add($my_right_menu, "sidebar_container");

   $my_content = new cHTML($my_page_data);
   $my_page->add($my_content, "content");

   $my_job_data = array (
    "directory"             => "./jobs/",
    "list_file"        => "jobs.txt",
    "class"            => "block_list"
   );

    $my_job_menu = new cCollection($my_job_data);
    $my_job_menu->id = "sidebar";
    $my_page->add($my_job_menu, "sidebar_container");


  $my_bib_data = array(
                        "directory" => "",
                        "bib_file"  => "publications/publications.bib"
                       ) ;

   $local_template = array(
                                  0 => "title",
                                  1 => "toc",
                                  2 => "author",
                                  3 => "journal",
                                  4 => "volume",
                                  5 => "pages",
                                  6 => "year",
                                  7 => "pdf",
                                  8 => "doi",
                                  9 => "abstract"
   );

   $my_bib = new cBIB($my_bib_data);
   $my_bib->id = "publications";
   $my_bib->home_dir = ".";
   $my_bib->output="highlight";
   $my_bib->style="./styles/publications.css";
   $my_bib->template = $local_template;
 
   $my_bib->filter_key = "type";
   $my_bib->filter_value = "highlight";
   $my_bib->header = "Publication highlights";
   $my_bib->subheadings = "no";
   $my_page->add($my_bib, "content");


   $my_footer = new cFooter($my_page_data);
   $my_page->add($my_footer, "footer");

   $my_page->display();

?>
