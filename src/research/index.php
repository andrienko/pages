<?php
   // Research page
   define("_HOME_DIR", "../");
   define("_XHTML_CLASS_DIR", _HOME_DIR."classes/");
   define("_XHTML_CLASS", _XHTML_CLASS_DIR."cPage.php");

   if (is_readable(_XHTML_CLASS)) include_once(_XHTML_CLASS);

   $my_page = new cPage(_HOME_DIR, "Research", "styles/index.css");

   $my_page_data = array (
                        "title"                 => "Research",
                        "directory"             => _HOME_DIR,
                        "navigator_file"        => "navigator.txt",
                        "menu_file"             => "menu.txt",
                        "content_file"          => "content.html",
                        "footer_text"           => "Copyright  Denis Andrienko, 2005"
                    );

   $my_navigator = new cNavigator($my_page_data);
   $my_page->add($my_navigator, "navigator");

   $my_right_data = array (
                        "directory"             => "",
                        "list_file"          => "projects.txt",
                    );


   $my_right_menu = new cCollection($my_right_data);
   $my_right_menu->id = "sidebar";
   $my_page->add($my_right_menu, "sidebar_container");

   $my_content = new cHTML($my_page_data);
   $my_page->add($my_content, "content");


   $my_footer = new cFooter($my_page_data);
   $my_page->add($my_footer, "footer");

   $my_page->display();


   ?>

