<?php

   // BPAPC research page
   define("_HOME_DIR", "../../../");
   define("_XHTML_CLASS_DIR", _HOME_DIR."classes/");
   define("_XHTML_CLASS", _XHTML_CLASS_DIR."cPage.php");

   if (is_readable(_XHTML_CLASS)) include_once(_XHTML_CLASS);

   $my_page = new cPage(_HOME_DIR, "Polycarbonates", "styles/index.css");

   $my_page_data = array (
                        "title"                 => "Polycarbonates",
                        "directory"             => _HOME_DIR,
                        "navigator_file"        => "navigator.txt",
                        "menu_file"             => "menu.txt",
                        "content_file"          => "content.html",
                        "footer_text"           => "Copyright  Denis Andrienko, 2005"
                    );

   $my_header = new cHeader($my_page_data);
   $my_page->add($my_header, "header");

   $my_menu = new cMenu($my_page_data);
   $my_menu->style = "styles/menu.css";
   $my_page->add($my_menu, "left_column");

   $my_content = new cHTML($my_page_data);
   $my_content->style = "styles/research.css";
   $my_page->add($my_content, "middle_column");

   $my_projects_data = array (
                        "directory"             => "../../",
                        "navigator_file"        => "projects.txt",
                    );

   $my_navigator = new cNavigator($my_page_data);
   $my_page->add($my_navigator, "navigator");

   $my_projects_data = array (
                        "directory"             => "../../",
                        "list_file"        => "projects.txt",
                    );

   $my_projects_menu = new cCollection($my_projects_data); 
   $my_page->add($my_projects_menu, "right_column");

   $project_data = array (
                        "directory"             => "",
                        "list_file"        => "menu.txt"
                    );

   $project_menu = new cCollection($project_data);
   $my_page->add($project_menu, "right_column");


   $my_footer = new cFooter($my_page_data);
   $my_page->add($my_footer, "footer");

   $my_page->display();

?>
