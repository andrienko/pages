#!/bin/bash

cp $1 $1.bak

gs \
  -o output.pdf \
  -sDEVICE=pdfwrite \
  -dFirstPage=2 \
  $1

mv output.pdf $1
