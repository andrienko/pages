<?php

// Publications page
define("_HOME_DIR", "../");
define("_XHTML_CLASS_DIR", _HOME_DIR . "classes/");
define("_XHTML_CLASS", _XHTML_CLASS_DIR . "cPage.php");

if (is_readable(_XHTML_CLASS))
    include_once(_XHTML_CLASS);

$my_page = new cPage(_HOME_DIR, "Publications", "styles/index.css");

$my_page_data = array(
    "title" => "Publications",
    "directory" => _HOME_DIR,
    "navigator_file" => "navigator.txt",
    "content_file" => "content.html",
    "footer_text" => "Copyright &copy Denis Andrienko."
);

$my_header = new cHeader($my_page_data);
$my_page->add($my_header, "header");

$my_navigator = new cNavigator($my_page_data);
$my_page->add($my_navigator, "navigator");

$bib_menu_data = array(
    "directory" => "",
    "list_file" => "menu.txt"
);

$bib_menu = new cCollection($bib_menu_data);
$bib_menu->id = "sidebar";
$my_page->add($bib_menu, "sidebar_container");

$my_bib_data = array(
    "directory" => "",
    "bib_file" => "publications.bib"
);

$local_template = array(
    0 => "title",
    1 => "author",
    2 => "journal",
    3 => "volume",
    4 => "pages",
    5 => "year",
    #6 => "pdf",
    6 => "doi",
    7 => "abstract"
);

$my_bib = new cBIB($my_bib_data);
$my_bib->template = $local_template;
$my_bib->id = "publications";
$my_bib->style = "styles/publications.css";
$my_bib->home_dir = "../";

if (isset($_GET['sort_key'])) {
    $my_bib->sort_key = $_GET['sort_key'];
}

// parse the first argument
parse_str($argv[1], $arg);
if(isset($arg['sort_key'])) {
    $my_bib->sort_key = $arg['sort_key'];
}

if (isset($_GET["filter_key"])) {
    $my_bib->filter_key = $_GET['filter_key'];
}

if (isset($_GET['filter_value'])) {
    $my_bib->filter_value = $_GET['filter_value'];
}

$my_page->add($my_bib, "content");

$my_footer = new cFooter($my_page_data);
$my_page->add($my_footer, "footer");

$my_page->display();
?>
