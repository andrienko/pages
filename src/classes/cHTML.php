<?php

include_once("cObject.php");

class cHTML extends cObject {

    var $id = "";
            
    var $required_args = array(
        "content_file"
    );

    function display() {
        echo "\n<!-- CHTML -->\n";
        
        // if id is specified, put div around the HTML output
        if (strcmp($this->id, "") != 0) {
            echo "<div class=\"$this->id\">\n";
        }
        
        include($this->options[$this->required_args[0]]);
        
        // if id is specified, put div around the HTML output
        if (strcmp($this->id, "") != 0) {
            echo "</div>\n";
        }
    }

}

?>
