<?php

class cHeader extends cObject {

    var $required_args = array(
        "title"
    );

    function display() {
        echo "<p>{$this->options["title"]}</p>\n";
    }

}

?>
