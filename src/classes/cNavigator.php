<?php

include_once("cList.php");

class cNavigator extends cList {

    var $required_args = array(
        "directory",
        "navigator_file"
    );

    function display() {
        $file = "{$this->options["directory"]}{$this->options["navigator_file"]}";
        $this->parse_file($file);

        foreach ($this->records as $key => $dataArray) {
            foreach ($dataArray as $k => $v) {
                echo "    <li><a href=\"{$this->options[$this->required_args[0]]}{$v}\">{$k}</a></li>\n";
            }
        }
    }

}

?>
