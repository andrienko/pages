<?php

class cObject {

    var $options = array();        // a list of options passed through the constructor
    var $position;                  // place where this object is placed on the main page: menu, header, etc
    var $style = "";                // additional style file to display the object
    var $class = "";                // class of the <div> </div> element


    // constructor
    public function __construct($data) {
        $calling_class = get_class($this);
        $calling_class_vars = get_class_vars($calling_class);

        // check if the needed fields were provided and put them into $data
        foreach ($calling_class_vars["required_args"] as $key) {
            if (array_key_exists($key, $data)) {
                $this->options[$key] = $data[$key];
            } else {
                $this->error("You must specify a $key!");
            }
        }
    }

    // shall be ovverriden by a particular object
    function display() {
        $this->error("Did you forget to override the display() method?");
    }

    // returns error message and the object name
    function error($message) {
        $class = get_class($this);
        die("\nERROR: $class: $message\n");
    }

}

?>
