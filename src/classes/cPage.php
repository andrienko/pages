<?php

include_once("classes.inc");

//main class for the xhtml page
class cPage {

    var $directory = "";       // relative directory
    var $title = "";       // page title
    var $common_title = "Organic Electronics MPIP Mainz"; // added to the title (for google search)
    var $style = "";       // style file
    var $icon = "";
    var $content = array(); // container for the objects
    var $keywords = "organic, electronics, charge, transport, MPIP, Mainz, Max Planck Institute for Polymer Research";
    var $description = "";
    var $author = "";
    var $bleft = False;
    var $bright = False;
    var $bmiddle = False;

    // constructor
    public function __construct($directory, $title, $style) {
        $this->directory = $directory;
        $this->title = $title;
        $this->style = $style;
    }

    function add($object, $position) {
        $object->position = $position;
        $this->content[] = $object;
        if (strcmp($position, "left_column")) {
            $this->bleft = True;
        }
        if (strcmp($position, "right_column")) {
            $this->bright = True;
        }
        if (strcmp($position, "middle_column")) {
            $this->bmiddle = True;
        }
    }

    function display_head() {
        echo "<!DOCTYPE html>\n";
        echo "<head>\n<title>$this->title - $this->common_title</title>\n";

        // responsive css design
        echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \n";
        
        echo "<meta charset=\"UTF-8\"> \n";
        echo "<meta name=\"author\" content=\"$this->author\" /> \n";
        echo "<meta name=\"keywords\" content=\"$this->keywords\" /> \n";
        echo "<meta name=\"description\" content=\"$this->description\" /> \n";

        $style_file = "{$this->directory}{$this->style}";
        //echo "DEBUG  $style_file  \n";

        if (file_exists($style_file)) {
            echo "<link rel=\"stylesheet\" href=\"$style_file\"/>\n";
        } else {
            $object = get_class($this);
            echo "<!-- $object: $this->style file is missing -->\n";
        }

        // add the style files of the rest of the objects
        reset($this->content);
        foreach( $this->content as $key => $obj ) {
        //while (list($key, $obj) = each($this->content)) {
            if (strcmp($obj->style, "") > 0) {
                $style_file = "{$this->directory}{$obj->style}";
                echo "<link rel=\"stylesheet\" href=\"$style_file\"/>\n";
            }
        }

        /*
        echo "<link rel=\"shortcut icon\" href=\"{$this->icon}\"/>\n";
        echo "<script type=\"text/javascript\" src=\"js/modernizr-1.5.min.js\"></script>\n";
         */
        echo "</head>\n";
    }

    // header of the page: logo, navigator  
    function display_header() {
        echo "<header class=\"main-head\">\n";
        echo "<div id=\"logo\">\n";
        echo "<div id=\"logo_text\">\n";
        echo "<h1><a href=\"\">Organic electronics<span class=\"logo_colour\"></span></a></h1>\n";
        echo "<h2>Theory and simulations</h2>\n";
        echo "</div>\n";
        echo "</div>\n";
        echo "</header>\n";
        
        # navigtor
        echo "<!-- navigator -->\n";
        echo "<nav class=\"main-nav\">\n";

        //echo "<div id=\"menu_container\">\n";
        //echo "<ul class=\"sf-menu\" id=\"nav\">\n";
        echo "<ul id=\"nav\">\n";
        reset($this->content);
        foreach( $this->content as $key => $obj ) {
        //while (list($key, $obj) = each($this->content)) {
            if ($obj->position == "navigator") {
                $obj->display();
            }
        }
        echo "</ul>\n";
        echo "</nav>\n";
        //echo "</div>\n";



    }

    // site content; sidebars
    function display_site_content() {
        //echo "<div id=\"main\"> <!-- main body -->\n";
        // includes content and sidebar
        //echo "<div id=\"site_content\"> <!-- site content -->\n";

        // main body of he page
        echo "<article class=\"content\"> <!-- content -->\n";
        //echo "<div id=\"content\"> <!-- content -->\n";
        reset($this->content);
        foreach( $this->content as $key => $obj ) {
        //while (list($key, $obj) = each($this->content)) {
            if ($obj->position == "content") {
                $obj->display();
            }
        }
        //echo "</div> <!-- end of the content -->\n";

        // sidebars        

        //echo "</div> <!-- end of the site content --> \n"; // site content
        echo "</article> <!-- end of the article --> \n"; // site content
        //echo "</div> <!-- end of them main -->\n"; // end of the main
        
        //echo "<div id=\"sidebar_container\"> <!-- sidebar -->\n";
        echo "<aside class=\"side\"> <!-- sidebar -->\n";
        reset($this->content);
        foreach( $this->content as $key => $obj ) {
        //while (list($key, $obj) = each($this->content)) {
            if ($obj->position == "sidebar_container") {
                $obj->display();
            }
        }
        echo "</aside> <!-- end of the aside -->\n";
        //echo "</div> <!-- end of the sidebar -->\n";
        
    }

    // footer
    function display_footer() {
        echo "<!-- footer -->\n";
        echo "<footer class=\"main-footer\"> ";
        reset($this->content);
        foreach( $this->content as $key => $obj ) {
        //while (list($key, $obj) = each($this->content)) {
            if ($obj->position == "footer") {
                $obj->display();
            }
        }
        echo "</footer>\n";
        echo "<!-- end of footer -->\n";
    }

    function display_end() {
        /*
        echo "<!-- javascript at the bottom for fast page loading -->\n";
        echo "<script type=\"text/javascript\" src=\"js/jquery.js\"></script>\n";
        echo "<script type=\"text/javascript\" src=\"js/jquery.easing-sooper.js\"></script>\n";
        echo "<script type=\"text/javascript\" src=\"js/jquery.sooperfish.js\"></script>\n";
        echo "<script type=\"text/javascript\">\n";
        echo "    $(document).ready(function() {\n";
        echo "    $('ul.sf-menu').sooperfish();\n";
        echo "    $(\'.top\').click(function() \{$(\'html, body\').animate(\{scrollTop:0\}, \'fast\'); return false;\});\n";
        echo "\});\n";
        echo "</script>\n";
        */
    }

    function display() {
        $this->display_head();
        echo "<body>\n";
        echo "<div class=\"wrapper\">\n";
        $this->display_header();
        $this->display_site_content();
        $this->display_footer();
        $this->display_end();
        echo "</div>\n";
        echo "</body>\n</html>";
    }

}
?>

<?php

/*
  $my_page = new cPage("", "Denis Andrienko", "index.css");

  $my_page_data = array (
  "title"         => "Denis Andrienko",
  "directory"      => "",
  "file"           => "menu.txt"
  );

  $my_skipmenu = new cSkipMenu($my_page_data);
  $my_page->add($my_skipmenu, "header");

  $my_navigator = new cNavigator($my_page_data);
  $my_page->add($my_navigator, "navigator");

  $my_menu = new cFloatMenu($my_page_data);
  $my_page->add($my_menu, "menu");

  $my_page_data = array (
  "file"           => "body.html"
  );
  $my_content = new cContent($my_page_data);
  $my_page->add($my_content, "content");

  $my_page_data = array (
  "title"           => "Copyright  Denis Andrienko 2005"
  );
  $my_footer = new cFooter($my_page_data);
  $my_page->add($my_footer, "footer");



  $my_page->display();
 */
?>
