<?php

include_once("cObject.php");

class cFooter extends cObject {

    var $required_args = array(
        "footer_text"
            );
    var $content_file = "content.html";

    function display() {

        print ("<p> <a href=\"http://www.mpip-mainz.mpg.de/2956/imprint\">Imprint</a> | 
                <a href=\"http://www.mpip-mainz.mpg.de/privacy_policy\">Privacy Policy</a> ");
        if (file_exists($this->content_file)) {
            $last_modified = filemtime($this->content_file);
            print ( " | Last modified ");
            print ( date("j/m/y", $last_modified));
        } else {
            echo("   {$this->options["footer_text"]}");
        }
    }

}

?>
