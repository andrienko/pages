<?php
include_once("cList.php");

class CCollection extends CList{

  var $required_args = array(
                                        "directory",
                                        "list_file"
                                  ) ;
  var $records;
  var $id;

  // outputs the formatted list 
  function display() {

      $file = "{$this->options[$this->required_args[0]]}{$this->options[$this->required_args[1]]}";
      $this->parse_file($file);

      echo "\n<!-- Ordered List -->\n";
      echo "<div class=\"$this->id\">\n";

           foreach ($this->records as $key=>$dataArray) {
             echo "<h1>{$key}</h1>\n";
             echo "  <ul>\n";
             foreach ($dataArray as $k => $v) {
                      echo "    <li><a href=\"{$this->options[$this->required_args[0]]}{$v}\">{$k}</a></li>\n";
             }
             echo "  </ul>\n";
           }           	

           echo "</div>";
  }

}

?>
