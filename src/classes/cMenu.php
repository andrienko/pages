<?php

include_once("cList.php");

class cMenu extends cList {
        var $required_args = array(
                                        "directory",
                                        "menu_file"
                                  ) ;
        function display() {

                   $file = "{$this->options[$this->required_args[0]]}{$this->options[$this->required_args[1]]}";
                   $this->parse_file($file);

                   echo "<div id =\"menu\">";
                   foreach ($this->records as $key=>$dataArray) {
                        echo "<h3>{$key}</h3>\n";
                        echo "  <ul>\n";
                        foreach ($dataArray as $k => $v) {

                                echo "    <li class=\"{$this->images[$k]}\"><a href=\"{$this->options[$this->required_args[0]]}{$v}\">{$k}</a></li>\n";
                        }
                        echo "  </ul>\n";
                   }
                   echo "</div>";

        }
}

?>
