<?php

include_once("cObject.php");

class cList extends cObject {

         var $records;
        // var $images;
        // parses the file in the .ini format to $records
        function parse_file($file) {
                if (file_exists($file)) {

                   $data = parse_ini_file($file, true);
                   foreach ($data as $section => $record) {
                        foreach ($record as $key => $value) {

                            $fields = preg_split('/\,/',$value);
                            //print_r( $fields );
                            $this->records[$section][$key] =  trim($fields[0]);
                            //$this->images[$key] = trim($fields[1]);
                        }
                   }
                } else {
                        $this->error("File $file is missing");
                }

        }
}

?>
