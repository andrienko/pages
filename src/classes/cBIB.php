<?php

include_once("cObject.php");

class cBIB extends cObject {

    var $required_args = array(
        "directory",
        "bib_file"
    );
    //  output of the records will be according to this template
    var $template = array(
        0 => "pdf",
        1 => "title",
        2 => "author",
        3 => "journal",
        4 => "volume",
        5 => "pages",
        6 => "year",
        7 => "doi",
        8 => "abstract"
    );
    var $n_records = 0;
    var $records;
    var $home_dir;
    var $sort_key = "year";
    var $filter_key = "";
    var $filter_value = "";
    var $output = "compact";
    var $header = "";
    var $subheadings = "yes";
    var $id = "";

    //get contents of a file into a string
    function parse_bib_file() {

        $bib_file = "{$this->options[$this->required_args[0]]}{$this->options[$this->required_args[1]]}";

        $fd = fopen($bib_file, "r");
        $contents = fread($fd, filesize($bib_file));
        fclose($fd);

        // splits the bib file on separate records
        $records = preg_split('/@/', $contents, -1, PREG_SPLIT_NO_EMPTY);

        $this->n_records = count($records);

        // splits each record on fields
        for ($i_record = 0; $i_record < $this->n_records; $i_record++) {

            // locates the id of each record
            $current_record = $records[$i_record];
            preg_match('/\{\s*\S*\,/', $current_record, $_id);
            $id = preg_replace('/[\{\s,]*/', '', $_id[0]);

            //print_r($id);
            //print "<br>";
            // splits each record on fields
            //$_record = preg_split('/,\s/', $current_record );
            preg_match_all('/\s*.*\s*[=]\s*[{]\s*.*\s*[}]/', $current_record, $matches);

            /*
              print_r($matches[0][0]);
              print "<br>";
              print_r($matches[0][1]);
              print "<br>";
              print_r($matches[0][2]);
              print "<br>";
              print_r($matches[0][3]);
              print "<br>";
              print_r($matches[0][4]);
              print "<br>";
              print_r($matches[0][5]);
              print "<br>";
              print_r($matches[0][6]);
              print "<br>";
              print_r($matches[0][7]);
              print "<br>";
              print_r($matches[0][8]);
              print "<br>";

              print "<br><b>Current record</b><br>";
              print_r($current_record);
              print "<br><b>Matches</b><br>";
              print_r($matches[0]);
              print "<br><br>";

             */
            $n_fields = count($matches[0]);

            //splits each field on the field_id and field_value
            for ($i_field = 0; $i_field < $n_fields; $i_field++) {

                // UNDEFINED OFFSET ERROR
                if (!array_key_exists($i_field, $matches[0])) {
                    print_r("<b>");
                    print_r($i_field);
                    print_r(" not found </b>");
                }
                $_field = preg_split('/=/', $matches[0][$i_field]);

                //print_r($_field);
                //print "<br>";

                $field_id = $_field[0];
                $field_id = strtolower(trim($field_id));
                $_field_value = $_field[1];
                $field_value = preg_replace('/[\{\}\",]*/', '', $_field_value);
                $field_value = preg_replace('/\\\\\'\\\/', '', $field_value);
                $field_value = trim($field_value);

                if (strcmp($field_id, "author") == 0) {
                    $field_value = preg_replace('/\s*( and )\s*/', ', ', $field_value);
                }

                $rec[$field_id] = $field_value;
                $this->records[$i_record][$field_id] = $field_value;
            }
            //print "<br><b>Record</b><br>";
            //print_r($this->records[$i_record]);
        }
    }

    function output_item($number, $template, $rec) {
        if (strcmp($this->output, "compact") == 0) {
            echo "<p class=\"publication\"> \n";
        }
        if (strcmp($this->output, "highlight") == 0) {
            echo "<div class=\"highlight\"> \n";
        }
        //print_r($rec);

        $nr = count($template);
        for ($i = 0; $i < $nr; $i++) {

            $key = $template[$i];

            // THIS PART IS UNCLEAR (ABSTRACT IS NOT FOUND)
            if (!array_key_exists($key, $rec)) {
                print_r("<b>");
                print_r($key);
                print_r(" not found </b>");
            }

            $value = $rec[$key];

            if (strcmp($this->output, "compact") == 0) {
                $this->output_compact($i, $key, $value, $nr);
            }

            if (strcmp($this->output, "highlight") == 0) {
                $this->output_abstract($i, $key, $value, $nr);
            }
        }
        if (strcmp($this->output, "compact") == 0) {
            echo "</p>\n";
        }
        if (strcmp($this->output, "highlight") == 0) {
            echo "</div>\n";
        }
    }

    // simple output, no abstract requested 
    function output_compact($i, $key, $value, $nr) {
        if (strlen($value) >= 1) {
            //echo  "$key, $value \n <br>";
            if (strcmp($key, "pdf") == 0) {
                echo "<a class=\"bib_pdf\" href=\"{$this->options[$this->required_args[0]]}{$value}\">";
                echo "<img src=\"{$this->home_dir}images/icon.pdf.red.gif\" border=\"0\" alt=\"[pdf]\"></a>\n";
                // doi
            } elseif (strcmp($key, "doi") == 0) {
                echo "<a  href=\"http://dx.doi.org/{$value}\">[doi]</a>\n";
                // title
            } elseif (strcmp($key, "title") == 0) {
                echo "<font class=\"{$key}\">{$value}</font><br>\n";
                // author
            } elseif (strcmp($key, "author") == 0) {
                echo "<font class=\"{$key}\">{$value}</font> <br>";
                //abstract
            } elseif (strcmp($key, "abstract") == 0) {
                $id = uniqid();
                echo "<a id=\"imageDivLink\" href=\"javascript:toggle('$id', 'imageDivLink');\">[abstract]</a>\n";
                echo "<div id=\"$id\" style=\"display: none;\">\n";
                echo "<font class=\"{$key}\">{$value}</font>\n";
                echo "</div>\n";
            } else {
                echo "<font class=\"{$key}\">{$value}</font>";
                if ($i < $nr - 1) {
                    echo ",\n";
                } else {
                    echo "\n";
                }
            }
        }
    }

    // output with abstract requested 
    function output_abstract($i, $key, $value, $nr) {
        if (strlen($value) >= 1) {
            if (strcmp($key, "toc") == 0) {
                echo "<span class=\"center\">\n";
                echo "<img width=\"280\" src=\"{$this->home_dir}/publications/toc/{$value}\" border=\"0\" alt=\"[toc]\">\n";
                echo "</span>\n";
                // title
            } elseif (strcmp($key, "title") == 0) {
                echo "<h2>$value</h2>\n";
                // author
            } elseif (strcmp($key, "author") == 0) {
                echo "{$value}, ";
            } elseif (strcmp($key, "journal") == 0) {
                echo "<i> {$value} </i>, ";
            } elseif (strcmp($key, "year") == 0) {
                echo "{$value} ";
            } elseif (strcmp($key, "doi") == 0) {
                echo "<a  href=\"http://dx.doi.org/{$value}\">[doi]</a>\n"; 
            } elseif (strcmp($key, "abstract") == 0) {
                echo "<p> <font class=\"{$key}\">{$value}</font></p>\n";
            } else {
                
            }
        }
    }

    function sort_bib_file($sort_key) {

        //echo "Sort key: $sort_key<br>";
        function cmp_year($a, $b) {
            return -$a["year"] + $b["year"];
        }

        function cmp_project($a, $b) {
            return strcmp($a["project"], $b["project"]);
        }

        function cmp_journal($a, $b) {
            return strcmp($a["journal"], $b["journal"]);
        }

        //echo " sorting with the key: <b>{$sort_key}</b> <br/> ";

        if (strcmp($sort_key, "year") == 0) {
            usort($this->records, "cmp_year");
        } elseif (strcmp($sort_key, "project") == 0) {
            usort($this->records, "cmp_project");
        } elseif (strcmp($sort_key, "journal") == 0) {
            usort($this->records, "cmp_journal");
        } else {
            
        }
    }

    function filter_bib_file($filter_key, $filter_value) {
        //echo "Filter key,value: $filter_key,$filter_value<br>;
        for ($i_record = 0; $i_record < $this->n_records; $i_record++) {

            // check whether the type field is specified in the bib file
            if (array_key_exists($filter_key, $this->records[$i_record])) {
                
                $value = $this->records[$i_record][$filter_key];
                stristr($value, $filter_value);

                if (stristr($value, $filter_value) == TRUE) {
                    //echo "found in $i_record: $value] <br> ";
                } else {
                    //echo "not found in $i_record: $value <br>";
                    unset($this->records[$i_record]);
                }
            } else { // not type is given in the bib record
                unset($this->records[$i_record]);
            }
        }

        if ($filter_value === "highlight" ) {
            // leaves only one random highlight in the list
            $selection = array_rand($this->records, 1);
            //echo "<b> $selection </b> <br>";
        
            foreach ($this->records as $key => $value) {
                //echo "<i> $key </i> <br>";
                if ( !( $selection === $key ) ) {
                    //echo "$selection, $key <br>";
                    unset($this->records[$key]);
                }            
            }
        }
    }

    function display() {
        // echo "parsing bib file\n";
        $this->parse_bib_file();

        $filter_key = $this->filter_key;
        $filter_value = $this->filter_value;
        if (strcmp($filter_key, "") != 0) {
            $this->filter_bib_file($filter_key, $filter_value);
        }

        $sort_key = $this->sort_key;
        $this->sort_bib_file($sort_key);

        echo "<script src=\"";
        echo $this->home_dir;
        echo "/js/toggle.js\"></script>\n";

        if (strcmp($this->header, "") != 0) {
            echo "<h1>$this->header</h1>\n";
        }

        echo "<div class= \"$this->id\" >\n";
        $records = sizeof($this->records);

        $section = "";
        
        for ($i_record = 0; $i_record < $records; $i_record++) {

            if (strcmp($section, $this->records[$i_record][$sort_key]) == 0) {
                // do not print section heading 
            } else
                if (strcmp($this->subheadings, "yes") == 0) {
                echo "<h1>{$this->records[$i_record][$sort_key]}</h1>";
            }
            // assign the current section name              
            $section = $this->records[$i_record][$sort_key];
            
            $record = $this->records[$i_record];
            $this->output_item($i_record, $this->template, $record);
        }
        echo "</div>\n";
    }

}

// end of the class xhtml_bib
?>
